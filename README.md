# Planeación del desarrollo de mini aplicación de blog

## Historias de usuario

- Yo como VISITANTE **quiero** consultar artículos **para** estudiar.
- Yo como VISITANTE **quiero** una lista ordenada de artículos **para** estudiar.
- Yo como VISITANTE **quiero** ver imagen y título de cada artículo **para** saber de qué trata.
- Yo como VISITANTE **quiero** ver los artículos ordenados por fechar **para** saber de qué tan actualizados están.
- Yo como VISITANTE **quiero** poder ver los botones de redes sociales **para** compartir los artículos que me gusten.
- Yo como VISITANTE **quiero** saber quién es la persona que publicó el artículo que me gusta **para** buscar más de él.
- Yo como VISITANTE **quiero** ver clasificados por categorías los artículos **para** buscar más de esa categoría.

-------------------------------------------------------------------------------
- Yo como ADMINISTRADOR **quiero** poder crear artículos.
- Yo como ADMINISTRADOR **quiero** poder leer artículos que yo he creado **para** saber como quedaron.
- Yo como ADMINISTRADOR **quiero** ver botones de acciones para artículos **para** poder eliminar o editar alguno de ellos.
- Yo como ADMINISTRADOR **quiero** poder compartir en redes sociales mis artículos **para** que la gente los conozca.

## SCHEMA
A continuación se presenta el **Schema** propuesto para el desarrollo del proyecto.

``` json
{
    "category": ["category1", "category2", "category3"],
    "title": "String",
    "author": {
        "name": "String",
        "age": "Number",
        "email": "String"
    },
    "created_at": "Date",
    "content": "String",
    "comments": [
        { "username": "String",  "content": "String"}
    ],
    "article_image": "String",
    "permalink": "String"
}
```
